# Shoot Enemy

This is puzzle game with enemy to defeat.

## Goals

- Defeat all enemies

## Features

* Enemies are spotted and patrol around the map
* When enemy detect player, shoot to kill player.

