// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UParticleSystem;
class USoundBase;
class UAnimMontage;

UCLASS()
class KILLZONE_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintPure)
	bool IsDeath() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	// Camera
	UPROPERTY(VisibleAnywhere, Category = "Camera")
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, Category = "Camera")
	UCameraComponent* CameraFollow;

	void Shoot();

private:
	// Movement
	void MoveForward(float rate);
	void MoveRight(float rate);

	void LookUp(float rate);
	void Turn(float rate);

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float TurnRate;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float LookUpRate;

	// Gun Shoot
	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

	UPROPERTY(EditAnywhere)
	float MaxRange = 1000.f;

	// Effects
	UPROPERTY(EditDefaultsOnly, Category = "Particles")
	UParticleSystem* MuzzleFlash = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Particles")
	UParticleSystem* ImpactEffect = nullptr;

	UPROPERTY(EditAnywhere)
	USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere)
	USoundBase* ImpactSound;

	// Health
	UPROPERTY(EditAnywhere, Category = "Health")
	float MaxHealth;

	UPROPERTY(VisibleAnywhere, Category = "Health")
	float Health;

	// Animation Montages
	UPROPERTY(EditAnywhere, Category = "Montage")
	UAnimMontage* FireMontage;

	// Damage System
	UPROPERTY(Editanywhere, Category = "Damage")
	float DamageTake;
};
