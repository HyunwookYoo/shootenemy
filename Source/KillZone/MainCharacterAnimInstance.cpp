// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacterAnimInstance.h"

#include "BaseCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

void UMainCharacterAnimInstance::UpdateAnimationProperties()
{
	if (!ensure(PlayerPawn != nullptr)) return;
	
	FVector Speed = Main->GetVelocity();
	FVector Direction = UKismetMathLibrary::InverseTransformDirection(PlayerPawn->GetActorTransform(), Speed);
	MovementSpeed = Speed.Size();
	Angle = Direction.Rotation().Yaw;

	FRotator Aim = UKismetMathLibrary::NormalizedDeltaRotator(PlayerPawn->GetControlRotation(), PlayerPawn->GetActorRotation());
	AimPitch = Aim.Pitch;

	bIsInAir = Main->GetCharacterMovement()->IsFalling();

	bIsDead = Main->IsDeath();
}

