// Copyright Epic Games, Inc. All Rights Reserved.

#include "KillZone.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, KillZone, "KillZone" );
