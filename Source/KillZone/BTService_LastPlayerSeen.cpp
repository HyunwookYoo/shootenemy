// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_LastPlayerSeen.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"

UBTService_LastPlayerSeen::UBTService_LastPlayerSeen()
{
	NodeName = TEXT("Last Player Location");
}

void UBTService_LastPlayerSeen::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (PlayerPawn == nullptr) return;

	AAIController* CharacterController = OwnerComp.GetAIOwner();
	if (CharacterController == nullptr) return;

	if (CharacterController->LineOfSightTo(PlayerPawn))
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), PlayerPawn);
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
	}
}
