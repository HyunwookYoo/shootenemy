// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "MainCharacterAnimInstance.generated.h"

class ABaseCharacter;

/**
 * 
 */
UCLASS()
class KILLZONE_API UMainCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Properties")
	void UpdateAnimationProperties();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float MovementSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float Angle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float AimPitch;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "MainCharacter", meta = (AllowPrivateAccess = "true"))
	APawn* PlayerPawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "MainCharacter", meta = (AllowPrivateAccess = "true"))
	ABaseCharacter* Main;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Jump", meta = (AllowPrivateAccess = "true"))
	bool bIsInAir;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Death", meta = (AllowPrivateAccess = "true"))
	bool bIsDead;
};
