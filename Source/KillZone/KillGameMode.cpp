// Fill out your copyright notice in the Description page of Project Settings.


#include "KillGameMode.h"

#include "EnemyAIController.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"

void AKillGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);
	
	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController != nullptr)
	{
		EndGame(false);
	}

	for (const auto& AIController : TActorRange<AEnemyAIController>(GetWorld()))
	{
		if (!AIController->IsDead())
		{
			return;
		}
	}

	EndGame(true);
}

void AKillGameMode::EndGame(bool bIsPlayerWinner)
{
	for (const auto& Controller : TActorRange<AController>(GetWorld()))
	{
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
