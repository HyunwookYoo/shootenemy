// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/MovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "KillZoneGameModeBase.h"
#include "Components/CapsuleComponent.h"

#define OUT

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 400.f;
	CameraBoom->bUsePawnControlRotation = true;

	CameraFollow = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraFollow"));
	CameraFollow->SetupAttachment(CameraBoom);
	LookUpRate = 15.f;
	TurnRate = 15.f;

	MaxHealth = 100.f;
	Health = MaxHealth;

	DamageTake = 20.f;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &ABaseCharacter::Shoot);

	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("Right"), this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &ABaseCharacter::LookUp);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ABaseCharacter::Turn);
}

float ABaseCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApplied = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	DamageToApplied = FMath::Min(Health, DamageToApplied);
	Health -= DamageToApplied;

	UE_LOG(LogTemp, Warning, TEXT("Health remain %f"), Health);

	if (IsDeath())
	{
		AKillZoneGameModeBase* GameMode = GetWorld()->GetAuthGameMode<AKillZoneGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageToApplied;
}

bool ABaseCharacter::IsDeath() const
{
	return Health <= 0;
}

float ABaseCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

void ABaseCharacter::MoveForward(float rate)
{
	FRotator Rotation = GetController()->GetControlRotation();
	FRotator YawRotation = FRotator(0.f, Rotation.Yaw, 0.f);

	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, rate);
}

void ABaseCharacter::MoveRight(float rate)
{
	FRotator Rotation = GetController()->GetControlRotation();
	FRotator YawRotation = FRotator(0.f, Rotation.Yaw, 0.f);

	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(Direction, rate);
}

void ABaseCharacter::LookUp(float rate)
{
	AddControllerPitchInput(LookUpRate * rate * GetWorld()->GetDeltaSeconds());
}

void ABaseCharacter::Turn(float rate)
{
	AddControllerYawInput(TurnRate * rate * GetWorld()->GetDeltaSeconds());
}

bool ABaseCharacter::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	AController* OwnerController = GetController();
	if (!ensure(OwnerController != nullptr)) return false;

	FVector ViewPointLocation;
	FRotator ViewPointRotation;
	OwnerController->GetPlayerViewPoint(OUT ViewPointLocation, OUT ViewPointRotation);
	FVector End = ViewPointLocation + (ViewPointRotation.Vector() * MaxRange);

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	
	ShotDirection = -ViewPointRotation.Vector();

	return GetWorld()->LineTraceSingleByChannel(OUT Hit, ViewPointLocation, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

void ABaseCharacter::Shoot()
{
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, GetMesh(), TEXT("Muzzle_01"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, GetMesh(), TEXT("Muzzle_01"));

	FHitResult Hit;
	FVector ShotDirection;

	if (GunTrace(OUT Hit, OUT ShotDirection))
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, Hit.Location);
		AActor* HitActor = Hit.GetActor();
		if (HitActor != nullptr)
		{
			FPointDamageEvent DamageEvent(DamageTake, Hit, ShotDirection, nullptr);
			HitActor->TakeDamage(DamageTake, DamageEvent, GetController(), this);
		}

		
	}
	
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (!ensure(AnimInstance != nullptr)) return;

	if (AnimInstance && FireMontage)
	{
		AnimInstance->Montage_Play(FireMontage, 1.8f);
	}
}

