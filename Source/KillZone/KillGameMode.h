// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KillZoneGameModeBase.h"
#include "KillGameMode.generated.h"

/**
 * 
 */
UCLASS()
class KILLZONE_API AKillGameMode : public AKillZoneGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void PawnKilled(APawn* PawnKilled) override;

private:
	void EndGame(bool bIsPlayerWinner);
};
